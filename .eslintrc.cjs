module.exports = {
  // 设置我们的运行环境为浏览器 + es2021 + node ,否则eslint在遇到 Promise，window等全局对象时会报错
  "env": {
    "browser": true,
    "es2021": true,
    "node": true
  },
  // 继承的语法包
  extends: [
    // 继承eslint推荐的规则集
    "eslint:recommended",
    // "plugin:vue/essential",
    "plugin:vue/vue3-recommended",
    // 继承eslint推荐的typescript的规则集
    "plugin:@typescript-eslint/recommended",
    'standard',
    "plugin:prettier/recommended" // 新增，必须放在最后面
  ],
  overrides: [],
  // 新增，解析vue文件
  "parser": "vue-eslint-parser",
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
    // 解析ts语法的
    "parser": "@typescript-eslint/parser",
    //设置 ts的配置文件
    // "project": ["./tsconfig.json"]
  },
  plugins: [
    'vue', "@typescript-eslint"
  ],
  rules: {
    "@typescript-eslint/ban-types": [
      "error",
      {
        "extendDefaults": true,
        "types": {
          "{}": false
        }
      }
    ],
    'vue/multi-word-component-names': 'off'
  }
}
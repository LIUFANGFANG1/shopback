// 模拟三种环境情况

// 第一种用哪种环境就揭开注释

// 1、开发环境
// export const base_url='https://bjwz.bwie.com/mall4w/dev';
// export const base_name='2001';

// 2、生产环境
// export const base_url='https://bjwz.bwie.com/mall4w/pro';
// export const base_name='2001';

// 3、测试环境
// export const base_url='https://bjwz.bwie.com/mall4w/';
// export const base_name='2001';

// 第二种
// console.log(process.env.NODE_ENV);
// let base_url='';
// let base_name='';
// if(process.env.NODE_ENV==='development'){
//     base_url='https://bjwz.bwie.com/mall4w/dev';
//     base_name='2001'
// }else if(process.env.NODE_ENV==='production'){
//     base_url='https://bjwz.bwie.com/mall4w/pro';
//     base_name='2001'
// }else if (process.env.NODE_ENV==='test') {
//     base_url='https://bjwz.bwie.com/mall4w/test';
//     base_name='2001'
// }
// export  {base_url,base_name};

// 第三种
// .env
// .env.production
// .env.development

export {};

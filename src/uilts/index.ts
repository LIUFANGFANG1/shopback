import axios from 'axios';
// import {base_name,base_url} from './config'
// 自定义配置新建一个实例
// baseURL是干哈的？ 根路径与get请求中的路径拼接起来
// 环境 ：开发环境development  测试环境test 预发布环境 生产环境production

console.log(import.meta.env.VITE_BASE_URL, 'import.meta.env');

const httpRequest = axios.create({
  // baseURL: base_url,
  baseURL: import.meta.env.VITE_BASE_URL,
  timeout: 1000,
});
export default httpRequest;

// 试一试 url===》 http://127.0.0.1:5173/api/login
// httpRequest.get('/login')

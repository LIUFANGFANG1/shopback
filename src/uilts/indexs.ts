// "noImplicitAny":false, 关闭模块校验
import HttpRequest from './request/httpRequest';

const httpRequest = new HttpRequest({
  baseURL: import.meta.env.VITE_BASE_URL,
  timeout: 1000,
});

export default httpRequest;

// 用法案例
// const login=()=>httpRequest.get({url:'/login',data:parmas})

import axios, { AxiosInstance, AxiosRequestConfig } from 'axios';
import { ElMessage, ElLoading } from 'element-plus';
import { LoadingInstance } from 'element-plus/lib/components/loading/src/loading';
const defaultLoading = true;
interface httpRequestConfig extends AxiosRequestConfig {
  isShowLoading?: boolean;
}
// 封装一个类
class HttpRequest {
  intance: AxiosInstance;
  loading?: LoadingInstance;
  // 后期自己加的  用继承的方式
  isShowLoading?: boolean;
  // config ==>在indexs.ts传进来的参数
  constructor(config: httpRequestConfig) {
    this.intance = axios.create(config); // 创建了axios实例
    // 判断config里面传没传加载中的状态
    this.isShowLoading = config.isShowLoading || false;
    // 添加请求拦截器
    this.intance.interceptors.request.use(
      (config) => {
        // 在发送请求之前做些什么   有一个加载中的状态 避免显示弹框
        if (this.isShowLoading) {
          this.loading = ElLoading.service({
            lock: true,
            text: 'Loading...',
            background: 'rgba(0, 0, 0, 0.7)',
          });
        }
        this.isShowLoading = false;
        return config;
      },
      function (error) {
        // 对请求错误做些什么
        return Promise.reject(error);
      }
    );
    // 添加响应拦截器
    this.intance.interceptors.response.use(
      (response) => {
        // 2xx 范围内的状态码都会触发该函数。
        // 对响应数据做点什么
        console.log(response);
        // 关闭加载中的状态
        this.loading?.close();
        this.isShowLoading = defaultLoading;
        return response;
      },
      (error) => {
        // 超出 2xx 范围的状态码都会触发该函数。
        // 对响应错误做点什么
        console.log(error);
        // 关闭加载中的状态
        // 解决不同接口请求显示
        this.isShowLoading = defaultLoading;
        this.loading?.close();

        if (error.response.status === 400) {
          ElMessage.error(error.response.data || '请求错误');
        } else if (error.response.status === 401) {
          ElMessage.error(error.response.data || '请求错误');
        } else {
          ElMessage.error('请求失败');
        }
        return Promise.reject(error);
      }
    );
  }

  //  request自己封装的
  request<T>(config: httpRequestConfig): Promise<T> {
    // 返回值是一个promise
    // this.intance.request(config).then(res=>{
    //     console.log(res);
    // })
    return new Promise((resolve, reject) => {
      // this.intance自带request
      this.intance
        .request<T, any>(config)
        .then((res) => {
          // console.log(res);
          resolve(res);
        })
        .catch((err) => {
          // 捕获异常
          reject(err);
        });
    });
  }

  get<T>(config: httpRequestConfig): Promise<T> {
    return this.request<T>({ ...config, method: 'GET' });
  }

  post<T>(config: httpRequestConfig): Promise<T> {
    return this.request<T>({ ...config, method: 'POST' });
  }
}

export default HttpRequest;

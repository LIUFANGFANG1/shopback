// 传啥，data类型就是啥类型
// <ResponseData<LoginData>> ====>相当于data的类型是LoginData
export interface ResponseData<T> {
  status: number;
  statusText: string;
  data: T;
}
export interface LoginData {
  access_token: string;
  authorities: any;
  expires_in: number;
  refresh_token: string;
  shopId: number;
  token_type: string;
  userId: number;
}

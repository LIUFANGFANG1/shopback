import httpRequest from '../uilts/indexs';
import { ResponseData, LoginData } from './type.d';
// export const login=(params:any)=>httpRequest.post<{
//     status:number;
//     statusText:string;
//     data:any;
// }>({
//     url:'login?grant_type=admin',
//     data:params
// })
export const login = (params: any) =>
  httpRequest.post<ResponseData<LoginData>>({
    url: 'login?grant_type=admin',
    data: params,
  });
